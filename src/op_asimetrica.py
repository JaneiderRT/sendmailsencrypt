"""
Este modulo se encarga de implementar el proceso de criptografia asimetrica
"""
from src.cryptografia import asimetrica
from src.mail import sendmail
from src.archivos import files, properties

def funcionalidad_asimetrica():
    """
    Contiene toda la logica para el flujo del programa en la implementacion de la criptografia
    asimetrica.
    """
    print("Menu Opciones Criptografia Asimetrica:\n" +
          "1). Generar Llaves.\n" +
          "2). Cifrar Informacion.\n" +
          "3). Descifrar Informacion.\n" +
          "4). Salir.\n")

    ruta_absoluta = files.obtener_ruta_conf()
    ruta_archivo  = properties.get_propiedad('ruta_default', ruta_absoluta + '/conf.properties')
    opcion        = int(input("Ingrese una opcion a continuacion ==> "))

    if opcion:
        if opcion == 1:
            res_nombre_llaves = input("\n¿Desea generar el nombre del archivo de llaves con sus iniciales como prefijo? S/n: ")

            if res_nombre_llaves:
                if res_nombre_llaves == "s" or res_nombre_llaves == "S":
                    nombre_llaves = input("\nPor favor ingrese las iniciales de sus nombres y apellidos Eje -> JRO: ")

                    if nombre_llaves:
                        print("\nGenerando Las llaves...")
                        asimetrica.generar_llaves(ruta_archivo, nombre_llaves)
                    else:
                        print("\nNo ingresaste el prefijo de las llaves, se generaran con un prefijo por defecto.")
                        print("Generando Las llaves...")
                        asimetrica.generar_llaves(ruta_archivo)
                else:
                    print("\nGenerando Las llaves con un prefijo por defecto...")
                    asimetrica.generar_llaves(ruta_archivo)
            else:
                print("\nNo respondiste, las llaves se generaran con un prefijo por defecto.")
                print("Generando Las llaves...")
                asimetrica.generar_llaves(ruta_archivo)
        elif opcion == 2:
            informacion = input("\nPor favor, a continuacion ingrese el mensaje que desea cifrar:\n\n")

            if informacion:
                nom_llave_receptor = input("\nPor favor, ingrese el prefijo del archivo que contiene la llave publica del receptor: ")

                if nom_llave_receptor:
                    existe_archivo = files.validar_existencia(ruta_archivo + nom_llave_receptor + "_pub.pem")

                    if existe_archivo:
                        llave_receptor = asimetrica.obtener_llave_receptor(ruta_archivo, nom_llave_receptor)
                    else:
                        print("\nEl archivo de llave publica del receptor no existe en la ruta configurada.\nSaliendo...")
                        return 1
                else:
                    existe_archivo = files.validar_existencia(ruta_archivo + "key_pub.pem")

                    if existe_archivo:
                        llave_receptor = asimetrica.obtener_llave_receptor(ruta_archivo)
                    else:
                        print("\nEl archivo de llave publica del receptor no existe en la ruta configurada.\nSaliendo...")
                        return 1

                print("\nEncriptando su informacion...")
                mensaje_encriptado = asimetrica.encriptar(informacion, llave_receptor)
                print("Su Mensaje ha sido encriptado satisfactoriamente\n\n")

                print("El mensaje encriptado se alamacenara en un archivo: .bin")
                name_file_encrypt = input("¿Cómo desea que se llame el archivo?: ")
                files.escribir_archivo(mensaje_encriptado, ruta_archivo + name_file_encrypt + ".bin")
                print("Se ha generado el archivo " + name_file_encrypt + ".bin" + " correctamente")

                respuesta = input("\n\n¿Desea enviar la informacion por correo? S/n: ")

                if respuesta:
                    if respuesta == 's' or respuesta == 'S':
                        correo_personal     = input("\n\nIngrese su direccion de correo electronico con el que desea enviar el mensaje: ")
                        correo_destinatario = input("Ingrese el correo del destinatario: ")
                        asunto_correo       = input("Ingrese el asunto del correo: ")
                        mensaje             = "Correo Secreto: Para descifrar el mensaje, use su llave privada."

                        if correo_personal:
                                if correo_destinatario:
                                    if asunto_correo:
                                        envio_mensaje = sendmail.enviar_email_adjunto(correo_personal, correo_destinatario, asunto_correo, mensaje, ruta_archivo, name_file_encrypt + ".bin")

                                        if envio_mensaje == 0:
                                            print("¡Mensaje enviado exitosamente!")
                                    else:
                                        print("Debe especificar un asunto para el correo.")
                                else:
                                    print("Debe especificar el correo del destinatario.")
                        else:
                            print("Debe especificar su correo.")
                else:
                    print("\nNo has dado ninguna respuesta, por lo que no se enviara el correo.")
            else:
                print("\nNo especificaste el mensaje a encriptar.\nSaliendo...")
        elif opcion == 3:
            name_file_cifrado = input("\nPor favor, a continuacion ingrese el nombre del archivo que contiene el mensaje cifrado sin la extensión: ")

            if name_file_cifrado:
                existe_mensaje = files.validar_existencia(ruta_archivo + name_file_cifrado + ".bin")

                if existe_mensaje:
                    iniciales_llave_privada = input("\nPor favor, ingrese el prefijo de su llave privada: ")

                    if iniciales_llave_privada:
                        existe_archivo = files.validar_existencia(ruta_archivo + iniciales_llave_privada + "_priv.pem")

                        if existe_archivo:
                            nom_llave_privada = asimetrica.obtener_llave_privada(ruta_archivo, iniciales_llave_privada)
                        else:
                            print("\nNo se encontro su llave privada en la ruta configurada, verifique su existencia.\nSaliendo...")
                            return 1
                    else:
                        existe_archivo = files.validar_existencia(ruta_archivo + "key_priv.pem")

                        if existe_archivo:
                            nom_llave_privada = asimetrica.obtener_llave_privada(ruta_archivo)
                        else:
                            print("\nNo se encontro su llave privada en la ruta configurada, verifique su existencia.\nSaliendo...")
                            return 1

                    info_cifrada = files.leer_archivo(ruta_archivo + name_file_cifrado + ".bin")

                    print("Desencriptando su mensaje...")
                    info_descifrada = asimetrica.desencriptar(info_cifrada, nom_llave_privada)
                    print("Su mensaje ha sido desencriptado satisfactoriamente\n")
                    print("Su mensaje es:\n" + info_descifrada)
                else:
                    print("\nNo existe el archivo que contiene el mensaje cifrado en la ruta configurada.\nSaliendo...")
            else:
                print("\nNo especificaste el nombre del archivo que contiene el mensaje cifrado.\nSaliendo...")
        elif opcion == 4:
            print("\nSaliendo...")
    else:
        print("\nNo especificaste una decision.\nSaliendo...")




#generar_llaves()
#llave_pub, llave_priv = obtener_llaves()
#
#mensaje = input("Ingrese el mensaje que desea encriptar: ")
#mensaje_cifrado = encriptar(mensaje, llave_pub)
#
#firma = firmar_sha1(mensaje, llave_priv)
#
#mensaje_descifrado = desencriptar(mensaje_cifrado, llave_priv)
#
#print("Su mensaje cifrado es:\n")
#print(mensaje_cifrado)
#print("\nSu firma es:\n")
#print(firma)
#print("\nSu mensaje descifrado es:\n" + mensaje_descifrado)
#
#if verificar_firma(mensaje_descifrado, firma, llave_pub):
#    print("Firma verificada satisfactoriamente.")
#else:
#    print("Desconocemos la firma.")