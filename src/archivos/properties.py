"""
Este modulo se encarga de la manipulacion de los archivos de configuración.
"""
import jprops
from src.archivos import files

def get_propiedades(nombre_archivo: str):
    """
    Obtiene un diccionario con todas las propiedades almacendas en el archivo especificado.

    Parametros
    ----------
    nombre_archivo : str -> ruta absoluta donde se encuentra el archivo.

    Retorna
    -------
    dict_propiedades : dict -> Diccionario que contiene todas las propiedades del archivo.
    """
    archivo_propiedades = files.abrir_archivo(nombre_archivo)
    dict_propiedades    = jprops.load_properties(archivo_propiedades)
    return dict_propiedades


def get_propiedad(clave_propiedad: str, nombre_archivo: str):
    """
    Obtiene una propiedad especificica, que contiene el archivo especificado. En caso de no existir la
    propiedad, se controla la excepcion KeyError con un mensaje informativo.

    Parametros
    ----------
    clave_propiedad : str -> La clave que contiene el valor de la propiedad.
    nombre_archivo  : str -> ruta absoluta donde se encuentra el archivo.

    Retorna
    -------
    propiedad : str -> El valor de la propiedad.
    """
    propiedades = {}
    propiedades = get_propiedades(nombre_archivo)

    if propiedades:
        try:
            propiedad = propiedades[clave_propiedad]
        except KeyError:
            print("Por favor, verifique si la clave que ingreso es correcta o si en el archivo de propiedades existe dicha clave.")

    return propiedad