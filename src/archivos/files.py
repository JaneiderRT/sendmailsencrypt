"""
Este modulo se encarga de la manipulacion de los archivos.
"""
import os

def escribir_archivo(contenido: bytes | str, ruta_abs_archivo: str, modo_apertura: str = 'b'):
    """
    El archivo especificado se abre por defecto en modo escritura y en binario (wb), para luego escribir el 
    contenido especificado.

    Parametros
    ----------
    contenido        : bytes | str -> Se refiere a lo que se va a escribir dentro del archivo especificado.
    ruta_abs_archivo : str -> Ruta donde se encuentra el archivo a escribir.
    modo_apertura    : str -> Se refiere al modo en el que se va a escribir el contenido en el archivo.

    Modos De Apertura
    -----------------
    'b'	Modo binario (predeterminado).

    't'	Modo texto.

    Ejemplo
    -------
    Escribe cualquier texto en un archivo.
    >>> escribir_archivo("Escribir este texto.", "D:/Users/archivo.txt", modo_apertura='t')
    """
    with open(ruta_abs_archivo, "w" + modo_apertura) as f:
        f.write(contenido)


def leer_archivo(ruta_abs_archivo: str, modo_apertura: str = 'b'):
    """
    El archivo especificado se abre por defecto en modo lectura y en binario (rb), para luego obtener el 
    contenido del archivo.

    Parametros
    ----------
    ruta_abs_archivo : str -> Ruta donde se encuentra el archivo a leer.
    modo_apertura    : str -> Se refiere al modo en el que se va a leer el contenido del archivo.

    Modos De Apertura
    -----------------
    'b'	Modo binario (predeterminado).

    't'	Modo texto.

    Retorna
    -------
    contenido : bytes | str -> Se refiere a la informacion que se lee del archivo especificado.

    Ejemplo
    -------
    >>> leer_archivo("D:/Users/archivo.txt", modo_apertura='t')
    Este es el contenido del archivo.
    """
    with open(ruta_abs_archivo, "r" + modo_apertura) as f:
        contenido = f.read()
    return contenido


def abrir_archivo(ruta_archivo: str):
    """
    Se abre el archivo especificado por defecto en modo lectura y texto (rt).

    Parametros
    ----------
    ruta_archivo : str -> Ruta donde se encuentra el archivo a abrir.

    Retorna
    -------
    archivo : TextIOWrapper -> Objeto legible similar a un archivo.
    """
    return open(ruta_archivo)


def obtener_ruta_conf():
    """
    Obtiene la ruta de configuraciones.
    """
    if os.path.abspath(os.getcwd()).__contains__('src'):
        ruta_conf = os.path.normpath(os.getcwd() + os.sep + os.pardir).replace('\\', '/')
    else:
        ruta_conf = os.path.abspath(os.getcwd()).replace('\\', '/')
    return ruta_conf


def validar_existencia(ruta_absoluta: str):
    """
    Valida si el archivo especificado existe.
    """
    archivo = ruta_absoluta
    return os.path.isfile(archivo)