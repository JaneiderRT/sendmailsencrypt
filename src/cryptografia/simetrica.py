"""
Este modulo se encarga del manejo de criptografia simetrica
"""
from cryptography.fernet import Fernet

def encriptar(mensaje: str, llave_compartida: bytes):
    """
    Codifica y cifra la informacion o mensaje con la llave simetrica.

    Parametros
    ----------
    mensaje          : str   -> El mensaje o informacion que se desea cifrar.
    llave_compartida : bytes -> La llave unica simetrica.

    Retorna
    -------
    mensaje_ecriptado : bytes -> El mensaje cifrado.
    """
    obj_fernet = Fernet(llave_compartida)
    mensaje_ecriptado = obj_fernet.encrypt(str.encode(mensaje))
    return mensaje_ecriptado


def desencriptar(mensaje_encriptado: bytes, llave_compartida: bytes):
    """
    Descifra y decodifica la informacion o mnesaje con la llave simetrica.

    Parametros
    ----------
    mensaje_encriptado : bytes -> El mensaje cifrado.
    llave_compartida   : bytes -> La llave unica simetrica.

    Retorna
    -------
    mensaje_decodificado : str -> El mensaje en texto plano.
    """
    obj_fernet = Fernet(llave_compartida)
    mensaje_desencriptado = obj_fernet.decrypt(mensaje_encriptado)
    mensaje_decodificado = mensaje_desencriptado.decode()
    return mensaje_decodificado


def generar_llave():
    """
    Genera la llave unica simetrica.
    """
    key = Fernet.generate_key()
    return key