"""
Este modulo se encarga del manejo de criptografia asimetrica
"""
import rsa
from rsa.key import PrivateKey, PublicKey

def generar_llaves(ruta: str, iniciales_nombre: str = 'key'):
    """
    Genera el par de llaves en la ruta e iniciales del propietario especificados.

    Parametros
    ----------
    ruta             : str -> Ruta en donde se guardan el par de llaves.
    iniciales_nombre : str -> Iniciales del nombre del propietario de las llaves. (Por defecto = 'key').
    """
    (llave_publica, llave_privada) = rsa.newkeys(2048)
    __guardar_llaves(llave_publica, llave_privada, ruta, iniciales_nombre)
    print("Se han generado las llaves correctamente.")


def obtener_llave_receptor(ruta: str, iniciales_llave: str = 'key'):
    """
    Obtiene la llave publica del receptor, para cifrar el mensaje que desea enviar.

    Parametros
    ----------
    ruta             : str -> Ruta en donde se encuentra la llave publica del receptor.
    iniciales_llave  : str -> Iniciales del nombre del propietario de la llave publica, en este caso el receptor. (Por defecto = 'key').

    Retorna
    -------
    llave_pub_receptor : AbstractKey -> Objeto llave con referencia a la llave publica del receptor.
    """
    with open(ruta + iniciales_llave + "_pub.pem", 'rb') as file_pub_receptor:
        llave_pub_receptor = rsa.PublicKey.load_pkcs1(file_pub_receptor.read())
    return llave_pub_receptor


def obtener_llave_privada(ruta: str, iniciales_llave: str = 'key'):
    """
    Obtiene la llave privada del emisor, para descifrar los mensajes que se reciben.

    Parametros
    ----------
    ruta             : str -> Ruta en donde se encuentra la llave privada del emisor.
    iniciales_llave  : str -> Iniciales del nombre del propietario de la llave privada, en este caso el emisor. (Por defecto = 'key').

    Retorna
    -------
    llave_privada : AbstractKey -> Objeto llave con referencia a la llave privada del emisor.
    """
    with open(ruta + iniciales_llave + "_priv.pem", 'rb') as file_priv_propia:
        llave_privada = rsa.PrivateKey.load_pkcs1(file_priv_propia.read())
    return llave_privada


def encriptar(mensaje: str, llave_receptor: PublicKey):
    """
    Codifica y cifra el mensaje o informacion especificada con la llave publica del receptor.

    Parametros
    ----------
    mensaje        : str       -> El mensaje o informacion que se desea cifrar.
    llave_receptor : PublicKey -> La llave publica del receptor.

    Retorna
    -------
    mensaje_encriptado : bytes -> El mensaje cifrado.
    """
    mensaje_encriptado = rsa.encrypt(mensaje.encode(), llave_receptor)
    return mensaje_encriptado


def desencriptar(mensaje_encriptado: bytes, llave_privada: PrivateKey):
    """
    Descifra y decodifica el mensaje o la informacion cifrada especificada con la llave privada del emisor.

    Parametros
    ----------
    mensaje_encriptado : bytes      -> El mensaje cifrado.
    llave_privada      : PrivateKey -> La llave privada del emisor.

    Retorna
    -------
    mensaje_desencriptado : str | False -> El mensaje en texto plano. 
    """
    try:
        mensaje_desencriptado = rsa.decrypt(mensaje_encriptado, llave_privada)
        return mensaje_desencriptado.decode()
    except:
        return False


def firmar_sha1(mensaje: str, llave_privada: PrivateKey):
    """
    Firma el mensaje con la llave privada del emisor.

    Parametros
    ----------
    mensaje       : str        -> El mensaje en texto plano.
    llave_privada : PrivateKey -> La llave privada del emisor.

    Retorna
    -------
    firma : bytes -> La firma del emisor.
    """
    firma = rsa.sign(mensaje.encode(), llave_privada, 'SHA-1')
    return firma


def verificar_firma(mensaje: str, firma: bytes, llave_pub_emisor: PublicKey):
    """
    Verifica que el mensaje recibido realmente sea del emisor.

    Parametros
    ----------
    mensaje            : str       -> El mensaje en texto plano.
    firma              : bytes     -> La firma del emisor.
    llave_pub_receptor : PublicKey -> La llave publica del emisor.

    Retorna
    -------
    verificado : str | False -> El nombre del hash utilizado.
    """
    try:
        verificado = rsa.verify(mensaje.encode(), firma, llave_pub_emisor)
        return verificado
    except:
        return False


def __guardar_llaves(llave_publica: PublicKey, llave_privada: PrivateKey, ruta_archivo: str, iniciales_nombre: str):
    """
    almacena cada una de las llaves generadas en un archivo y guarda el archivo en la ruta especificada.
    """
    with open(ruta_archivo + iniciales_nombre + "_pub.pem", 'wb') as file_pub:
        file_pub.write(llave_publica.save_pkcs1())

    with open(ruta_archivo + iniciales_nombre + "_priv.pem", 'wb') as file_priv:
        file_priv.write(llave_privada.save_pkcs1())