"""
Este modulo se encarga de implementar el proceso de criptografia simetrica
"""
from src.cryptografia import simetrica
from src.mail import sendmail
from src.archivos import files, properties

def funcionalidad_simetrica():
    """
    Contiene toda la logica para el flujo del programa en la implementacion de la criptografia
    simetrica.
    """
    print("Menu Opciones Criptografia Simetrica:\n" +
          "1). Generar Llave.\n" +
          "2). Cifrar Informacion.\n" +
          "3). Descifrar Informacion.\n" +
          "4). Salir.\n")

    ruta_absoluta = files.obtener_ruta_conf()
    ruta_archivo  = properties.get_propiedad('ruta_default', ruta_absoluta + '/conf.properties')
    opcion = int(input("Ingrese una opcion a continuacion ==> "))

    if opcion:
        if opcion == 1:
            print("\nGenerando la llave...")
            key = simetrica.generar_llave()
            files.escribir_archivo(key, ruta_archivo + "llave_simetrica.bin")
            print("Se ha generado el archivo llave_simetrica.bin correctamente.")
        elif opcion == 2:
            informacion = input("\nPor favor, a continuacion ingrese la informacion que desea cifrar:\n\n")

            if informacion:
                existe_archivo = files.validar_existencia(ruta_archivo + "llave_simetrica.bin")

                if existe_archivo:
                    llave = files.leer_archivo(ruta_archivo + "llave_simetrica.bin")
                else:
                    print("\nDebes generar una llave simetrica porque el archivo de llave no existe.\nSaliendo...")
                    return 1

                if llave:
                    print("Encriptando su informacion...")
                    info_cifrada = simetrica.encriptar(informacion, llave)
                    print("Su informacion ha sido encriptada satisfactoriamente\n\n")

                    respuesta = input("¿Desea enviar la informacion por correo? S/n: ")

                    if respuesta:
                        if respuesta == 's' or respuesta == 'S':
                            correo_personal     = input("\n\nIngrese su direccion de correo electronico con el que desea enviar el mensaje: ")
                            correo_destinatario = input("Ingrese el correo del destinatario: ")
                            asunto_correo       = input("Ingrese el asunto del correo: ")
                            mensaje_encriptado  = info_cifrada.decode()

                            if correo_personal:
                                if correo_destinatario:
                                    if asunto_correo:
                                        envio_mensaje = sendmail.enviar_email_adjunto(correo_personal, correo_destinatario, asunto_correo, mensaje_encriptado, ruta_archivo, "llave_simetrica.bin")

                                        if envio_mensaje == 0:
                                            print("¡Mensaje enviado exitosamente!")
                                    else:
                                        print("Debe especificar un asunto para el correo.")
                                else:
                                    print("Debe especificar el correo del destinatario.")
                            else:
                                print("Debe especificar su correo.")
                        else:
                            print("\nTu informacion cifrada es:\n" + info_cifrada.decode())
                    else:
                        print("\nNo has dado ninguna respuesta, por lo que no se enviara el correo.")
                        print("Tu informacion cifrada es:\n" + info_cifrada.decode())
                else:
                    print("\nValida si el archivo contiene la llave o, vuelve a generar otra.\nSaliendo...")
            else:
                print("\nNo especificaste la informacion a cifrar.\nSaliendo...")
        elif opcion == 3:
            informacion_cifrada = input("\nPor favor, a continuacion ingrese la informacion que desea descifrar:\n\n")

            if informacion_cifrada:
                existe_archivo = files.validar_existencia(ruta_archivo + "llave_simetrica.bin")

                if existe_archivo:
                    llave = files.leer_archivo(ruta_archivo + "llave_simetrica.bin")
                else:
                    print("\nValida que el archivo llave_simetrica.bin que te compartieron se encuentre en tu ruta configurada.\nSaliendo...")
                    return 1

                if llave:
                    print("Desencriptando su informacion...")
                    info_descifrada = simetrica.desencriptar(str.encode(informacion_cifrada), llave)
                    print("Su informacion ha sido desencriptada satisfactoriamente\n")
                    print("Su información es:\n" + info_descifrada)
                else:
                    print("\nValida si el archivo contiene la llave o, solicita de nuevo la llave al remitente.\nSaliendo...")
            else:
                print("\nNo especificaste la informacion a descifrar.\nSaliendo...")
        elif opcion == 4:
            print("\nSaliendo...")
    else:
        print("\nNo especificaste una decision.\nSaliendo...")