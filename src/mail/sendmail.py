"""
Este modulo se encarga del envio de los correos electronicos
"""
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

def enviar_email_adjunto(correo_from: str, correo_to: str, asunto: str, mensaje: str, ruta_adjunto: str, nombre_archivo: str):
    """
    Envia correos con un archivo adjunto.

    Parametros
    ----------
    correo_from    : str -> Direccion de correo electronico del emisor.
    correo_to      : str -> Direccion de correo electronico del receptor.
    asunto         : str -> Asunto del correo.
    mensaje        : str -> Mensaje del correo.
    ruta_adjunto   : str -> Ruta en donde se encuentra el archvio a adjuntar.
    nombre_archivo : str -> Nombre del archivo a adjuntar.

    Retorna
    -------
    Literal[0, 1] -> ``0`` si no ocurre ningun error al enviar el mensaje; ``1`` si ocurre algun error al enviar el mensaje.
    """
    password = __obtener_pass_email()
    if password != None:
        server           = __conf_server(correo_from, password)
        mensaje_completo = __conf_adjunto(correo_from, correo_to, asunto, mensaje, ruta_adjunto, nombre_archivo)
        server.sendmail(correo_from, correo_to, mensaje_completo)
        server.quit()
        return 0
    else:
        print("No se puede enviar el mensaje, cree la variable de entorno PasswordEmail con su contraseña de correo electronico.")
        return 1


def __conf_server(correo_from: str, clave_correo: str):
    """
    Configura el protocolo del servidor de gmail para el envio de correos.
    """
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.starttls()
    server.login(correo_from, clave_correo)
    return server


def __conf_adjunto(correo_from: str, correo_to: str, asunto: str, mensaje: str, ruta_adjunto: str, nombre_archivo: str):
    """
    Configura el correo y el archivo adjunto que se va a enviar.
    """
    mime_mensaje            = MIMEMultipart()
    mime_mensaje['From']    = correo_from
    mime_mensaje['To']      = correo_to
    mime_mensaje['Subject'] = asunto

    mime_mensaje.attach(MIMEText(mensaje, 'plain'))

    with open(ruta_adjunto + nombre_archivo, 'rb') as f:
        archivo_adjunto = f.read()

    adjunto_mime = MIMEBase('application', 'octet-stream')
    adjunto_mime.set_payload(archivo_adjunto)
    encoders.encode_base64(adjunto_mime)
    adjunto_mime.add_header('Content-Disposition', 'attachment', filename=nombre_archivo)
    mime_mensaje.attach(adjunto_mime)

    mensaje_completo = mime_mensaje.as_string()

    return mensaje_completo


def __obtener_pass_email():
    """
    Obtiene la clave del correo del emisor.
    """
    var      = os.environ
    password = var.get('PasswordEmail')
    return password