from src import op_simetrica, op_asimetrica

print("Menu Opciones:\n" +
      "1). Usar Criptografia Simetrica.\n" +
      "2). Usar Criptografia Asimetrica.\n" +
      "3). Salir.\n")

try:
    opcion = int(input("Ingrese una opcion a continuacion ==> "))

    if opcion:
        if opcion == 1:
            print("\n**************************************************************\n" +
                  "                     Criptografia Simetrica                     \n" +
                  "**************************************************************")
            op_simetrica.funcionalidad_simetrica()
        elif opcion == 2:
            print("\n**************************************************************\n" +
                  "                     Criptografia Asimetrica                    \n" +
                  "**************************************************************")
            op_asimetrica.funcionalidad_asimetrica()
        else:
            print("\nSaliendo...")
    else:
        print("Debes seleccionar una opcion")
except ValueError:
    print("\n\nSolo se permite ingresar numeros.\nSaliendo...")
except KeyboardInterrupt:
    print("\n\nSaliendo...")