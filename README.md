# Envío De Mensajes Cifrados -> *Send Mails Encrypt*

Corre en lenguaje **Python** - versión 3.10.0 o posterior.

Este proyecto tiene como fin el envío de mensajes cifrados vía correo electrónico haciendo uso de tres tipos de criptografía:

1. Criptografía Simétrica
2. Criptografía Asimétrica
3. Criptografía Híbrida

## Tipos De Criptografía Usados (Explicación)
**Criptografía Simétrica:**

También conocida por _**Criptografía de clave simétrica**_, es el método criptográfico que consiste en cifrar y descifrar mensajes entre un emisor y un receptor con una única clave o llave.

<div align="center"><img src=img/criptografia_simetrica.png height=220></div>

Este método en comparación con los otros es el más rápido en cuanto al cifrado de mensajes; sin embargo, tiene como desventaja el uso de una única clave, puesto que esta debe ser compartida al receptor. Dado el caso en que el receptor y el emisor estén distanciados, al envíar la clave por medio de la red existe la posibilidad de que un atacante pueda obtener dicha clave y; por ende, descifrar el mensaje.

**Criptografía Asimétrica:**

También conocida por _**Criptografía de clave pública**_ o _**Criptografía de dos claves**_, es el método criptográfico que consiste en que una misma persona tenga dos claves; una pública que se debe compartir con las personas que necesitan enviarnos un mensaje cifrado y otra privada que no debe ser revelada nunca.

<div align="center"><img src=img/criptografia_asimetrica.png height=220></div>

Por ejemplo, el emisor tiene sus propias claves y el receptor también tiene sus propias claves. El receptor le comparte su clave pública al emisor; con lo que el emisor teniendo la clave pública del receptor puede cifrar el mensaje y enviarselo; una vez el receptor tenga el mensaje cifrado que le envío el emisor, podrá descifrar dicho mensaje con su clave privada.

Este método en comparación con la (**Criptografía Simétrica**), es mucho más seguro por el manejo de dos tipos de claves que tiene cada usuario. A simple vista puede parecer que al saber la clave pública se podrá deducir la clave privada, pero no es correcto; puesto que, este método genera a partir de una frase de paso las dos claves que pueden llegar a tener un tamaño de 2048 bits; lo que hace que este método sea probablemente imposible de reventar.

Sin embargo, tiene como desventaja el tiempo de cifrado y descifrado de mensajes; es decir, que en comparación con el anterior método de criptografía, es mucho más lento al cifrar y descifrar la información.

**Criptografía Híbrida:**

Este método consiste en unir las ventajas de los dos anteriores métodos criptográficos y; por consiguiente, suplir las desventajas de los mismos.

Por lo que para su implementación, se debe hacer uso de los dos anteriores métodos criptográficos de la siguiente manera:

**Emisor:**

- Debe contar con la clave pública del receptor.
- Debe generar una clave simétrica.
- Debe cifrar el mensaje con criptografía simétrica y con la llave que se generó.
- Debe cifrar la llave simétrica con la llave pública del receptor.
- Debe envíar el mensaje cifrado de forma simétrica y la clave simétrica cifrada de forma asimétrica al receptor.

**Receptor:**

- Debe descifrar la llave simétrica con su propia llave asimétrica privada.
- Debe descifrar el mensaje cifrado con la llave simétrica.

## Uso

Primeramente se debe descargar el proyecto.

- [INSTALACIÓN PYTHON](#instalación-python)

- [INSTALACIÓN MÓDULOS](#instalación-de-los-módulos)

- [CONFIGURACIÓN DE LA VARIABLE DE ENTORNO PARA EMAIL](#configuración-de-la-variable-de-entorno-para-email)

- [CONFIGURACIÓN RUTA ARCHIVOS](#configuración-ruta-archivos)

- [CONFIGURACIÓN EJECUCIÓN ADMIN CMD](#configuración-ejecución-admin-cmd)

- [ACCESO APPS POCO SEGURAS CORREO](#acceso-apps-poco-seguras-correo)

- [EJECUCIÓN DEL PROGRAMA](#ejecución-del-programa)

### INSTALACIÓN PYTHON

Para su ejecución se debe inicialmente tener instalado **Python** - versión 3.10.0 o posterior.

- Se puede descargar el instalador desde su página oficial: https://www.python.org/

Hay que tener en cuenta que en el proceso de instalación de **Python**, se debe seleccionar el check que corresponde a la configuración automática de las rutas de python en la variable de entorno **PATH**.

<div align="center"><img src=img/setup_python.png height=300></div>

Una vez se tenga instalado **Python**, se debe validar que en la variable de entorno del usuario **PATH**, se encuentre configuradas las rutas correspondientes de **Python**.

<div align="center"><img src=img/variables_entorno.png height=450></div>

<div align="center"><img src=img/ruta_variable.png height=450></div>

### INSTALACIÓN DE LOS MÓDULOS

A continuación se debe abrir una terminal o CMD, y posicionarse dentro de la ruta del proyecto; una vez se esté a nivel del archivo **requirements.txt**, ejecutar el siguiente comando:

> **pip install -r requirements.txt**

El comando instalará los módulos necesarios para ejecutar correctamente el proyecto.

### CONFIGURACIÓN DE LA VARIABLE DE ENTORNO PARA EMAIL

Es necesario configurar la variable de entorno del sistema **PasswordEmail** que va a contener el código generado del registro de la aplicación para el uso del correo electrónico **(Revisar la sección de [ACCESO APPS POCO SEGURAS CORREO](#acceso-apps-poco-seguras-correo))**.

<div align="center"><img src=img/variable_correo.png height=450></div>

<div align="center"><img src=img/creacion_varcorreo.png height=150></div>

<div align="center"><img src=img/varcorreo_creada.png height=450></div>

A continuación, se debe oprimir en el botón "Aceptar" de las demás ventanas para que la variable quede guardada correctamente.

### CONFIGURACIÓN RUTA ARCHIVOS

En la ruta del proyecto, se encuentra un archivo de configuración denominado **conf.properties**.

Por defecto, contiene la ruta ``C:/Users/`` configurada. Si se desea configurar otra ruta, se debe:

- Abrir el archivo **conf.properties**.
- Especificar la ruta por defecto que deseé para la lectura y generación de archivos, como las llaves propias y mensajes cifrados.

> **Nota:** En la ruta que especifique en el archivo **conf.properties**, se deben dejar las llaves públicas que nos compartan otros usuarios.

> **Nota2:** En el caso de que se utilicé el programa en un sistema operativo **Windows**, la ruta debe tener como separadores el signo **/** y no el signo **\\**; también, se debe tener en cuenta que la ruta debe contener al final el signo antes mencionado.

De acuerdo a la **Nota2**, como ejemplo:

- La ruta correcta es   --> **D:/Usuarios/envio_mensajes/**
- La ruta incorrecta es --> **D:\Usuarios\envio_mensajes\\**

**Ejemplo:**

<div align="center"><img src=img/configuracion_properties.png height=350></div>

### CONFIGURACIÓN EJECUCIÓN ADMIN CMD

Cuando se configura una ruta que necesité permisos de administrador como lo son las rutas del disco ``C:/``, se necesita abrir el ``CMD``
en modo administrador.

Sin embargo; para evitar la tarea continua de abrir el CDM en modo administrador manualmente, se puede configurar de la siguiente manera:

1. Abrimos la ubicación del archvio:

<div align="center"><img src="img/buscar_cmd.png" height=350></div>

2. Abrimos las propiedades del acceso directo del cmd:

<div align="center"><img src="img/propiedades_cmd.png" height=350></div>

3. Seleccionamos ``Opciones avanzadas...``:

<div align="center"><img src="img/opciones_avanzadas_cmd.png" height=350></div>

4. Seleccionamos el check para ejecutar siempre como administrador el CMD:

<div align="center"><img src="img/conf_admin_cmd.png" height=350></div>

Con esto siempre al abrir el CMD, se ejecutará como ``Administrador``.

### ACCESO APPS POCO SEGURAS CORREO

Se debe configurar el correo o los correos que se desean utilizar para enviar los mensajes desde la aplicación.

1. Primeramente se debe ingresar al correo electrónico, para luego seleccionar la opción que nos permite administrar la cuenta:

<div align="center"><img src=img/opcion_correo.png height=350></div>

2. Seleccionar en el menú de la parte izquierda la opción **Seguridad**:

<div align="center"><img src=img/opcion_seguridad_correo.png height=350></div>

3. Deslizar el scroll hacía abajo, hasta que se visualicé la siguiente opción y seleccionar el apartado de la sub-opción **Contraseñas de aplicaciones**:

<div align="center"><img src=img/apps_poco_seguras.png height=350></div>

4. Debe configurar la verificación en dos pasos para continuar.

5. Se solicitará verificar la identidad con la contraseña del correo y seleccionamos las siguientes opciones:

<div align="center"><img src="img/password_aplicaciones.png" height=350></div>

Acontinuación ingresamos un nombre para la aplicación y oprimimos en **Generar**:

<div align="center"><img src="img/registrar_aplicacion.png" height=350></div>

Se genera el codigo que vamos a utilizar para la autenticación de la aplicacion **(copiar y pegar el codigo en la variable de entorno)**:

<div align="center"><img src="img/password_aplicacion.png" height=350></div>

### EJECUCIÓN DEL PROGRAMA

Se debe abrir una terminal o CMD, y posicionarse dentro de las carpetas sendmailsencrypt\src\ y ejecutar el siguiente comando:

> **python main.py**

A continuación debería ejecutarse exitosamente:

<div align="center"><img src=img/Ejecucion_Exitosa.png height=350></div>

> **Nota:** En caso de no funcionar el programa al ejecutarlo, se debe configurar las rutas de **Python** configuradas anteriormente, pero esta vez en la variable de entorno del sistema **PATH**.

## Soporte
Contactarse al: janeider.rojas@gmail.com

## Contribuir
Para contribuir, sigue las siguientes instrucciones:

1. Realizar un **fork** del proyecto.
2. Crear una rama (**branch**) con tus añadidos, por ejemplo (`git checkout -b nombre_rama`).
3. Realizar un **commit** con los cambios, por ejemplo (`git commit -m "iniciales_nombre - descripción_cambio"`).
4. Realizar un **push** a la rama creada, por ejemplo (`git push origin nombre_rama`).
5. Abrir una **Pull Request**.

## Créditos

- Imágenes explicación criptografías: https://www.genbeta.com/desarrollo/tipos-de-criptografia-simetrica-asimetrica-e-hibrida
- Proyecto analizador de archivos **.properties**: https://pypi.org/project/jprops/
- Proyecto de criptografía: https://pypi.org/project/cryptography/
- Proyecto de RSA: https://pypi.org/project/rsa/
- Envío de correos con adjunto: https://gist.github.com/2624789/d42aaa12bf3a36356342

## Licencia
[GPL v3](https://www.gnu.org/licenses/gpl-3.0).
